class User < ApplicationRecord
  rolify

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable, :validatable and
  # :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
         :validatable

  belongs_to :secret_code

  def admin?
    self.has_role? :admin
  end

end
