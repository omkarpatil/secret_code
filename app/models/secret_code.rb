class SecretCode < ApplicationRecord
  has_one :user

  scope :not_used, -> { where.not(id: User.pluck(:secret_code_id)) }

  SIZE_OPTIONS = [1, 10, 20, 50, 100]
  DEFAULT_SIZE_OPTION = 10

  def self.generate(size: DEFAULT_SIZE_OPTION)
    while !size.zero?
      duplicate = 0
      size.times do
        unless SecretCode.create(code: self.get_random_code)
          duplicate += 1
        end
      end
      size = duplicate
    end
    true
  end

  private

    def self.get_random_code
      SecureRandom.hex(3)
    end
end
