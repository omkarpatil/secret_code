class SecretCodesController < ApplicationController
  authorize_resource through: :current_user

  def index
    @secret_codes = SecretCode.all.page(page).per(per_page).includes(:user)
  end

  def generate
    if SecretCode.generate(size: params[:size].to_i)
      redirect_to secret_codes_path, notice: 'Codes created successfully'
    else
      redirect_to secret_codes_path, alert: 'Something went wrong!'
    end
  end

end
