class ApplicationController < ActionController::Base
  before_action :authenticate_user!

  def page
    params[:page].presence || 1
  end

  def per_page
    params[:per_page].presence || 10
  end
end
