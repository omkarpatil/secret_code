class AddSecretCodeIdColumnToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :secret_code_id, :integer
    add_index :users, :secret_code_id
  end
end
